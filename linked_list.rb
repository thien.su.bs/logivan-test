load 'node.rb'
class LinkedList
  attr_reader   :head

  def initialize
    @head= nil
    @tail= nil
    @n   = 0
  end

  def insert_head(value)
    node = Node.new(value)
    return nil if !node
    add_first(node)
    @n = @n + 1
    node
  end

  def insert_tail(value)
    node = Node.new(value)
    return nil if !node
    add_tail(node)
    @n = @n + 1
    node
  end

  def add_after(value_target, new_node)
    node           = find(value_target)
    if node
      new_node.next = node.next
      node.next = new_node
      if node == @tail
        @tail = new_node
      end
      @n+= 1
    else
      # Add first because the list is empty
      add_first(new_node)
      @n+= 1
    end
  end

  def find(value)
    return false unless @head # return false because the list is empty
    node = @head
    return node  if node.value == value # return node no need to accessing the list.
    while (node = node.next)
      return node if node.value == value
    end
  end

  def remove_head
    return nil if @head ==nil # empty list
    pick_head
  end

  def pick_after(target_value)
    node           = find(target_value)
    if node
      temp_node = node.next
      if temp_node == @tail
        @tail = node
      end
      node.next = temp_node.next
      temp_node = nil
      @n-= 1
    else
      nil
    end
  end

  def pick_node(target_value)
    return unless @head
    if @head.value == target_value
      @head= @head.next
      @n   = @n - 1
      @tail = nil if @n==0
      return @n
    end
    previous_node = find_before(target_value)
    if previous_node
      previous_node.next = previous_node.next.next
      @n                 = @n - 1
      @tail = nil if @n==0
      @n
    else
      nil
    end
  end

  def print
    node = @head
    return unless node
    puts node
    while (node = node.next)
      puts node
    end
  end

  def size
    @n
  end

  private
  def add_first(node)
    if @head == nil
      @head = node
      @tail = @head
    else
      node.next = @head
      @head     = node
    end
  end

  def add_tail(node)
    if @head == nil
      @head = node
      @tail = @head
    else
      @tail.next = node
      @tail      = node
    end
  end

  def pick_head
    if @head
      temp_head      = @head
      @head          = @head.next
      temp_head.next = nil
      @n             = @n - 1
      if @head==nil
        @tail= nil
        @n   = 0
      end
    end
  end

  def find_before(value)
    return false unless @head
    node = @head
    return false if !node.next
    return node  if node.next.value == value
    while (node = node.next)
      return node if node.next && node.next.value == value
    end
  end
end