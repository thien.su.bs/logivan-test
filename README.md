# logivan-test

This is read-me of project for test.

First at all, you should install gem rspec via:
```
bundle install -j 4
```

After that, you can run the rspec
```
rspec -fd
```

##More information:

```ruby
co = Checkout.new(["DISCOUNT_10", "BUY_2"])
co.scan('001')
co.scan('002')
co.scan('003')
```

###Test data
```
  Basket: 001,002,003
  Total price expected: £66.78
  Basket: 001,003,001
  Total price expected: £36.95
  Basket: 001,002,001,003
``` 
