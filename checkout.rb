load 'product_list.rb'
require_relative './util_checkout.rb'
class Checkout
  include UtilCheckout
  attr_accessor :items
  attr_accessor   :promotional_rules
  attr_accessor   :total_temp
  attr_accessor   :total
  def initialize(promotional_rules=[], option={})
    @items = []
    @promotional_rules = promotional_rules
    @total_temp = 0.00
    @total = 0.00
  end

  def to_s
    "Product with items: #{@items}"
  end

  # Scan each code, only add item with product code in DB
  def scan(code)
    item = @@products.find(code)
    return unless item
    @total_temp += UtilCheckout::scan_money(item.value.price)
    @items << item.value
  end


  # Returns the float total price
  def total_price
    return 0.00 unless @@products.head # return false because the list is empty
    return 0.00 if @items.empty?
    if promotional_rules.empty?
      @total = @total_temp.round(2) 
      return @total
    end

    if !is_valid_pro_buy_2 && !is_valid_pro_down_10
      @total = @total_temp.round(2) 
      return @total
    end

    total_final = 0.00
    
    # promotion buy more than 2 is higher priority
    # improvement, should we using tree-condition for promotion system
    # using Partitions items and get return items discount
    if is_valid_pro_buy_2
      @items.each do |item|
        node = @@products.head
        #Make sure the price of product.
        loop do
          if node.value.code == item.code
            price = @@promotions['BUY_2'].product_need.include?(item.code) ? @@promotions['BUY_2'].value : UtilCheckout::scan_money(node.value.price)
            total_final = total_final + price
            break
          end
          node = node.next
          break if node.nil?
        end
      end
      
      if is_valid_pro_down_10
        discount = @@promotions['DISCOUNT_10'].percentage.to_f/100
        total_final = total_final*(1 - discount)
      end
      @total = total_final.round(2)
      return @total
    #Valid of production
    elsif is_valid_pro_down_10
      discount = @@promotions['DISCOUNT_10'].percentage.to_f/100
      @items.each do |item|
        node = @@products.head
        #Make sure the price of product.
        loop do
          if node.value.code == item.code 
            total_final = total_final + UtilCheckout::scan_money(node.value.price) * (1-discount)
            break
          end
          node = node.next
          break if node.nil?
        end
      end
      @total = total_final.round(2)
      return @total
    end
  end

  private
  # Returns the true or false, which it apply Promotion DISCOUNT 10%
  # when spend orver - value
  def is_valid_pro_down_10
    @total_temp > @@promotions['DISCOUNT_10'].value && promotional_rules.include?('DISCOUNT_10') ? true : false
  end
  
  # Returns the true or false, which it apply Promotion BUY2
  # when buy more then number of item ['001', '001'] or 
  # ['002', '002'] by input of rule
  def is_valid_pro_buy_2
    cp_items =  @items.map{|e| e.code}
    return false if @@promotions['BUY_2'].product_need.size < 2
    return false unless promotional_rules.include?('BUY_2')
    all_in = true
    @@promotions['BUY_2'].product_need.each do |p_need|
      if cp_items.include?(p_need)
        p_index = cp_items.index(p_need)
        cp_items.delete_at(p_index)
      else
        all_in = false
      end
    end
    all_in
  end
end