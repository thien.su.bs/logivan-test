load 'linked_list.rb'
class ProductList < LinkedList 
  def find(product_code)
    return false unless @head # return false because the list is empty
    node = @head
    return node  if node.value.code == product_code # return node no need to accessing the list.
    while (node = node.next)
      return node if node.value.code == product_code
    end
  end
end