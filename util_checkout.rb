module UtilCheckout
  def self.scan_money(input)
    input.gsub(/[^\d\.]/, '').to_f
  end
end