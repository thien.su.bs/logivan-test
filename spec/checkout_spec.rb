load 'product.rb'
load 'checkout.rb'
require "ostruct"
describe 'Checkout Flow' do 
  before(:all) do 
    @@products = ProductList.new
    product_1 = Product.new('001', 'Lavender heart', '£9.25')
    product_2 = Product.new('002', 'Personalised cufflinks ', '£45.00')
    product_3 = Product.new('003', 'Kids T-shirt', '£19.95')
    @@products.insert_tail(product_1)
    @@products.insert_tail(product_2)
    @@products.insert_tail(product_3)
    @@promotions = {}
    promotion_1 = OpenStruct.new(code: 'DISCOUNT_10', status: true, product_need: [], value: 60.00, percentage: 10, level: 1, 
                                desc: "If you spend over £60, then you get 10% off of your purchase")
    promotion_2 = OpenStruct.new(code: 'BUY_2', status: true, product_need: ['001', '001'], value: 8.50, percentage: 0, level: 2, 
                                  desc: "If you buy 2 or more lavender hearts then the price drops to £8.50.")
    @@promotions["BUY_2"]= promotion_2
    @@promotions["DISCOUNT_10"]= promotion_1
  end

  it 'should created products' do 
    expect(@@products.size).to eq(3) 
  end

  it 'Basket: is empty' do
    co = Checkout.new()
    expect(co.items.size).to eq(0)
    expect(co.total_price).to eq(0.00)
  end

  it 'Basket: is empty with promotions' do
    co = Checkout.new(["DISCOUNT_10", "BUY_2"])
    expect(co.items.size).to eq(0)
    expect(co.total_price).to eq(0.00)
  end

  it 'Basket: is empty with promotion' do
    co = Checkout.new(["DISCOUNT_10"])
    expect(co.items.size).to eq(0)
    expect(co.total_price).to eq(0.00)
  end

  it 'Basket: is empty with promotion' do
    co = Checkout.new(["BUY_2"])
    expect(co.items.size).to eq(0)
    expect(co.total_price).to eq(0.00)
  end

  it 'Basket: 001 without promotion' do
    co = Checkout.new([])
    co.scan('001')
    expect(co.items.size).to eq(1)
    expect(co.total_price).to eq(9.25)
  end

  it 'Basket: 001 with promotion' do
    co = Checkout.new(['BUY_2'])
    co.scan('001')
    expect(co.items.size).to eq(1)
    expect(co.total_price).to eq(9.25)
  end

  it 'Basket: 001 with promotion GET 10% OFF' do
    co = Checkout.new(['DISCOUNT_10'])
    co.scan('001')
    expect(co.items.size).to eq(1)
    expect(co.total_price).to eq(9.25)
  end

  it 'Basket: 001 with promotions GET 10% OFF, BUY 2 MORE' do
    co = Checkout.new(['DISCOUNT_10, BUY_2'])
    co.scan('001')
    expect(co.items.size).to eq(1)
    expect(co.total_price).to eq(9.25)
  end

  it 'Basket: 001, 002 with promotion' do
    co = Checkout.new(['BUY_2'])
    co.scan('001')
    co.scan('002')
    expect(co.items.size).to eq(2)
    expect(co.total_price).to eq(54.25)
  end

  it 'Basket: 001,002 with promotion GET 10% OFF' do
    co = Checkout.new(['DISCOUNT_10'])
    co.scan('001')
    co.scan('002')
    expect(co.items.size).to eq(2)
    expect(co.total_price).to eq(54.25)
  end

  it 'Basket: 001,002 with promotions GET 10% OFF, BUY 2 MORE' do
    co = Checkout.new(['DISCOUNT_10, BUY_2'])
    co.scan('001')
    co.scan('002')
    expect(co.items.size).to eq(2)
    expect(co.total_price).to eq(54.25)
  end

  it 'Basket: some items are not in products list without promotions' do
    co = Checkout.new()
    co.scan('001')
    co.scan('002')
    co.scan('003')
    co.scan('006')
    expect(co.items.size).to eq(3)
    expect(co.total_price).to eq(74.2)
  end

  it 'Basket: some items are not in products list without promotions, re-order item scan' do
    co = Checkout.new()
    co.scan('006')
    co.scan('003')
    co.scan('001')
    co.scan('002')
    expect(co.items.size).to eq(3)
    expect(co.total_price).to eq(74.2)
  end
  
  it 'Basket: 001,002,003 with promotion GET 10% OFF' do 
    co = Checkout.new(["DISCOUNT_10"])
    co.scan('001')
    co.scan('002')
    co.scan('003')
    expect(co.items.size).to eq(3)
    expect(co.total_price).to eq(66.78)
  end

  it 'Basket: 001,002,003 with promotion BUY 2 MORE' do 
    co = Checkout.new(["BUY_2"])
    co.scan('001')
    co.scan('002')
    co.scan('003')
    expect(co.items.size).to eq(3)
    expect(co.total_price).to eq(74.2)
  end    

  it 'Basket: 001,002,003' do 
    co = Checkout.new(["DISCOUNT_10", "BUY_2"])
    co.scan('001')
    co.scan('002')
    co.scan('003')
    expect(co.total_price).to eq(66.78)
    puts "Total price expected: £66.78"
  end  

  it 'Basket: 001,003,001' do
    co = Checkout.new(["BUY_2", "DISCOUNT_10"])
    co.scan('001')
    co.scan('003')
    co.scan('001')
    expect(co.total_price).to eq(36.95)
    puts "Total price expected: £36.95"
  end 

  it 'Basket: 001,002,001,003' do
    co = Checkout.new(["BUY_2", "DISCOUNT_10"])
    co.scan('001')
    co.scan('002')
    co.scan('001')
    co.scan('003')
    expect(co.total_price).to eq(73.76)
    puts "Total price expected: £73.76"
  end

  it 'Basket: 001,002,001,003,001 with promotions GET 10% OFF, BUY 2 MORE' do
    co = Checkout.new(["BUY_2", "DISCOUNT_10"])
    co.scan('001')
    co.scan('002')
    co.scan('001')
    co.scan('003')
    co.scan('001')
    expect(co.items.size).to eq(5)
    expect(co.total_price).to eq(81.41)
    # puts "Total price expected: £73.76"
  end
end